# README #

Branches for all Fogarty Wines sites, using a customised version of the Elementy theme.

## config.yml

```yml
development:
  theme_id: 164264270
  password: 4b9966088e9e57ccf4f1439ab412bf12
  store: fogarty-wines.myshopify.com
  ignore_files:
  - config/settings_data.json
  - "*.png"
  - "*.jpg"
  - "*.mp4"
  - /\.(txt|gif|bat)$/
  - untitled file
  timeout: 90s
  refill_rate: 2
  bucket_size: 40
  concurrency: 4
bunkers:
  theme_id: 170588999
  password: deb3bbf5ef1c528a9019bf9ffbaa5dd0
  store: bunkers-2.myshopify.com
  ignores:
  ignore_files:
  - config/settings_data.json
  - "*.mp4"
  - "*.png"
  - "*.jpg"
  - /\.(txt|gif|bat)$/
  - untitled file
  timeout: 90s
  refill_rate: 2
  bucket_size: 40
  concurrency: 4
deepwoods:
  theme_id: 164385037
  password: 28474f6557849cf196729ba366b2dd74
  store: deep-woods-2.myshopify.com
  ignore_files:
  - config/settings_data.json
  - "*.png"
  - "*.jpg"
  - "*.mp4"
  - /\.(txt|gif|bat)$/
  - untitled file
  timeout: 90s
  refill_rate: 2
  bucket_size: 40
  concurrency: 4
lakesfolly:
  theme_id: 164094918
  password: 65e4bd5cac267277e6e3bc1e8201c6bb
  store: lakes-folly.myshopify.com
  ignore_files:
  - config/settings_data.json
  - "*.png"
  - "*.jpg"
  - "*.mp4"
  - /\.(txt|gif|bat)$/
  - untitled file
  timeout: 90s
  refill_rate: 2
  bucket_size: 40
  concurrency: 4
millbrook:
  theme_id: 163841222
  password: f9bec4c0eeb16d6fdf3030c0b32eed50
  store: millbrook-2.myshopify.com
  ignore_files:
  - config/settings_data.json
  - "*.png"
  - "*.jpg"
  - "*.mp4"
  - /\.(txt|gif|bat)$/
  - untitled file
  timeout: 90s
  refill_rate: 2
  bucket_size: 40
  concurrency: 4
smithbrook:
  theme_id: 171283917
  password: ca9cfda5be4cf908ed1c9a0c83082917
  store: smithbrook.myshopify.com
  ignore_files:
  - config/settings_data.json
  - "*.png"
  - "*.jpg"
  - "*.mp4"
  - /\.(txt|gif|bat)$/
  - untitled file
  timeout: 90s
  refill_rate: 2
  bucket_size: 40
  concurrency: 4
```